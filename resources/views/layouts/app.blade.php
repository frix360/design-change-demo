<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="https://colorlib.com/polygon/gentelella/index.html" class="site_title"><i
                                class="fa fa-paw"></i> <span> @lang('quickadmin.quickadmin_title')</span></a>
                </div>

                @include('partials.sidebar')

            </div>
        </div>

        @include('partials.topbar')

        <div class="right_col" role="main" style="min-height: 949px;">

            @if (Session::has('message'))
                <div class="note note-info">
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif
            @if ($errors->count() > 0)
                <div class="note note-danger">
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @yield('content')
        </div>
    </div>
</div>

{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}

@include('partials.javascripts')
</body>
</html>